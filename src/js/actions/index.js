import {ADD_TODO,DELETE_TODO,TOGGLE_TODO } from '../constants';

export const addTodo = (todoElem,toggled,todoId,nextTodoId) => {
	const action = {
		type: ADD_TODO,
		todoElem,
		toggled,
		todoId,
		nextTodoId,
	}
	return action;
}

export const deleteTodo = (newTodos,nextTodoId) => {
	const action = {
		type:DELETE_TODO,
		todoId:newTodos.todoId,
		nextTodoId,
	}
	return action;
}

export const toggleTodo = (newTodos,nextTodoId) =>{
	const action ={
		type:TOGGLE_TODO,
		todoElem:newTodos.todoElem,
		todoId:newTodos.todoId,
		toggled:newTodos.toggled,
		nextTodoId:nextTodoId
	}
	return action;
} 